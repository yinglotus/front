import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../components/Login'
import Index1 from '../components/Index1'
import Index2 from '../components/Index2'
import Index3 from '../components/Index3'
import Index4 from '../components/Index4'
import Index5 from '../components/Index5'
import PricingSelect from '../components/PricingSelect'

import Test from '../components/Test'
import DiscountsDevelopment from '../components/discountsDevelopment'

import Dealertiyan from '../components/dealertiyan'
import Dealertiyandetail from '../components/dealertiyandetail'

import Showweibo from '../components/weibo/Showweibo'
import SelectBrandShowweibo from '../components/weibo/SelectBrandShowweibo'
 
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  {
    path:'/login',
    name:'Login',
    component:Login
  },
  {
    path:'/index1',
    name:'Index1',
    component:Index1
  },
  {
    path:'/index2',
    name:'Index2',
    component:Index2
  },
  {
    path:'/index3',
    name:'Index3',
    component:Index3
  },
  {
    path:'/index4',
    name:'Index4',
    component:Index4
  },
  {
    path:'/index5',
    name:'Index5',
    component:Index5
  },
  {
    path:'/pricingSelect',
    name:'PricingSelect',
    component:PricingSelect
  },
  {
    path:'/discountsDevelopment',
    name:'discountsDevelopment',
    component:DiscountsDevelopment
  },
  {
    path:'/dealertiyan',
    name:'dealertiyan',
    component:Dealertiyan
  },
  {
    path:'/dealertiyandetail',
    name:'dealertiyandetail',
    component:Dealertiyandetail
  },

  {
    path:'/test',
    name:'Test',
    component:Test
  },
  {
    path:'/showweibo',
    name:'Showweibo',
    component:Showweibo
  },
  {
    path:'/selectBrandShowweibo',
    name:'SelectBrandShowweibo',
    component:SelectBrandShowweibo
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
